

//   为了使用await和async 必须把代码放入到一个函数中
//   放入到立即执行函数中 或 放入到页面加载事件中
//   load所有资源加载完触发,DOMContentLoaded是标签结构加载完毕触发
document.addEventListener('DOMContentLoaded', async function () {
  // ***********获取概览数据并渲染 **************/
  // 1.1  调用接口获取数据 (有权限认证的接口 要携带 token)
  const res = await axios({
    method: 'get',
    url: '/dashboard',
  })
  console.log('服务器响应的数据', res);
})