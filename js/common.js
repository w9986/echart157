// 上面这个代码处理过度动画（默认加上不用管）
document.addEventListener('DOMContentLoaded', () => {
  setTimeout(() => {
    document.body.classList.add('sidenav-pinned')
    document.body.classList.add('ready')
  }, 200)
})

function tip(msg = '默认提示内容') {
  //  第三步
  const toastBox = document.querySelector('#myToast')
  const toast = new bootstrap.Toast(toastBox, {
    animation: true,   // 开启动画效果
    autohide: true,   //  是否自动隐藏
    delay: 3000   //  3秒后关闭   --  多少毫秒
  })
  //  修改盒子中的提示内容
  toastBox.querySelector('.toast-body').innerHTML = msg
  toast.show()  //  调用
}
/***********设置axios全局的请求根地址************/
axios.defaults.baseURL = 'http://ajax-api.itheima.net'
//  展示用户名
//  获取盒子 从本地储存中取出用户明设置给盒子
const username = document.querySelector('.navbar-nav .font-weight-bold')
if (username) {
  username.innerHTML = localStorage.getItem('user-name')
}

//  1.退出登录 按钮
//  注册点击事件
const logout = document.querySelector('#logout')
if (logout) {
  logout.addEventListener('click', function () {
    //  2.移除本的储存 中的 token
    localStorage.removeItem('user-token')
    localStorage.removeItem('user-name')
    //  3.页面跳转登录页
    location.href = 'login.html'
  })
}

//*******axios的请求拦截器 */
// 添加请求拦截器
axios.interceptors.request.use(function (config) {
  // 在发送请求之前做些什么
  //  修改 配置信息 给请求头设置 token
  const token = localStorage.getItem('user-token')
  if (token) {
    config.headers.authorization = token
  }
  return config;  //  不能动
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error);
});


// 添加响应拦截器
axios.interceptors.response.use(function (response) {
  // 对响应数据做点什么
  // alert('拦截到了一个成功的响应')
  // console.log('拦截器中的 response', response); //
  return response.data
}, function (error) {
  // 对响应错误做点什么   如果失败则会执行该函数
  // alert('拦截到了一个失败的响应')

  // 统一处理token 验证失败的错误
  if (error.response.status === 401) {
    alert('token失效了,请重新登陆')
    //清除 token
    localStorage.removeItem('user-token')
    localStorage.removeItem('user-name')
    // 跳转到登录页
    location.href = 'login.html'
  }
  return Promise.reject(error);
});
